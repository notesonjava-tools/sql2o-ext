package com.notesonjava.sql2o;


import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ZonedDateTimeConverterTest {

	private ZonedDateTimeConverter converter = new ZonedDateTimeConverter();
	
	
	@Test
	public void toSqlDate(){
		ZonedDateTime time = ZonedDateTime.of(2018, 2, 10, 10,15,30, 0, ZoneOffset.UTC);
		Object result = (String)converter.toDatabaseParam(time);
		
		Assertions.assertThat(result.toString()).isEqualTo("2018-02-10T10:15:30.000Z");
	}
}
