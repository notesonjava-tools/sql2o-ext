package com.notesonjava.sql2o;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.sql2o.converters.Converter;
import org.sql2o.converters.ConverterException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ZonedDateTimeConverter implements Converter<ZonedDateTime> {
    @Override
    public ZonedDateTime convert(final Object val) throws ConverterException {
        if (val instanceof Date) {
        	log.debug("Convert to ZonedDateTime : " + val);
        	ZonedDateTime result = ((Date) val).toInstant().atOffset(ZoneOffset.UTC).toZonedDateTime();
        	log.debug("Convert to ZonedDateTime : " + val + " to "+ result.toString());    	
        	return result;
        } else {
            return null;
        }
    }

    @Override
    public Object toDatabaseParam(final ZonedDateTime val) {
        if (val == null) {
            return null;
        } else {
        	String result = val.format( DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz"));
        	log.debug("To Database param : " + val +" to "+ result.toString());
            return result;
        }
    }
}